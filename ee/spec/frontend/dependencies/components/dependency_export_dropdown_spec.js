import { GlButton } from '@gitlab/ui';
import { shallowMountExtended } from 'helpers/vue_test_utils_helper';
import DependencyExportDropdown from 'ee/dependencies/components/dependency_export_dropdown.vue';
import createStore from 'ee/dependencies/store';
import { DEPENDENCY_LIST_TYPES } from 'ee/dependencies/store/constants';
import {
  EXPORT_FORMAT_CSV,
  EXPORT_FORMAT_DEPENDENCY_LIST,
  EXPORT_FORMAT_JSON_ARRAY,
  NAMESPACE_GROUP,
  NAMESPACE_ORGANIZATION,
  NAMESPACE_PROJECT,
} from 'ee/dependencies/constants';

describe('DependencyExportDropdown component', () => {
  let store;
  let wrapper;

  const { namespace: allNamespace } = DEPENDENCY_LIST_TYPES.all;

  const factory = ({ provide, props } = {}) => {
    store = createStore();
    jest.spyOn(store, 'dispatch').mockImplementation();

    wrapper = shallowMountExtended(DependencyExportDropdown, {
      store,
      propsData: props,
      provide: {
        glFeatures: { groupDependencyListCsvExport: true },
        ...provide,
      },
    });
  };

  const findDisclosure = () => wrapper.findByTestId('export-disclosure');
  const findButton = () => wrapper.findComponent(GlButton);

  const dependencyListItem = {
    testId: 'dependency-list-item',
    exportType: EXPORT_FORMAT_DEPENDENCY_LIST,
  };
  const csvItem = {
    testId: 'csv-item',
    exportType: EXPORT_FORMAT_CSV,
  };
  const jsonArrayItem = {
    testId: 'json-array-item',
    exportType: EXPORT_FORMAT_JSON_ARRAY,
  };

  const itHasCorrectLoadingLogic = (selector) => {
    it('shows export icon in default state', () => {
      const attributes = selector().attributes();
      expect(attributes).toHaveProperty('icon', 'export');
      expect(attributes).not.toHaveProperty('loading', true);
    });

    describe('when request is pending', () => {
      beforeEach(() => {
        store.state[allNamespace].fetchingInProgress = true;
      });

      it('shows loading spinner', () => {
        expect(selector().attributes()).toMatchObject({
          icon: '',
          loading: 'true',
        });
      });
    });
  };

  const itShowsDisclosureWithItems = (items) => {
    it('shows disclosure with expected items', () => {
      expect(findDisclosure().exists()).toBe(true);
      items.forEach((item) => {
        expect(wrapper.findByTestId(item.testId).exists()).toBe(true);
      });
    });

    it('dispatches export when item is clicked', () => {
      items.forEach((item) => {
        wrapper.findByTestId(item.testId).vm.$emit('action');
        expect(store.dispatch).toHaveBeenCalledWith(`${allNamespace}/fetchExport`, {
          export_type: item.exportType,
        });
      });
    });
  };

  describe('when container is a project', () => {
    beforeEach(() => {
      factory({ props: { container: NAMESPACE_PROJECT } });
    });

    itHasCorrectLoadingLogic(() => findDisclosure());
    itShowsDisclosureWithItems([dependencyListItem, csvItem]);
  });

  describe('when container is a group', () => {
    beforeEach(() => {
      factory({ props: { container: NAMESPACE_GROUP } });
    });

    itHasCorrectLoadingLogic(() => findDisclosure());
    itShowsDisclosureWithItems([jsonArrayItem, csvItem]);

    describe('when group CSV export feature flag is disabled', () => {
      beforeEach(() => {
        factory({
          props: { container: NAMESPACE_GROUP },
          provide: { glFeatures: { groupDependencyListCsvExport: false } },
        });
      });

      it('shows button that dispatches JSON export', () => {
        const button = findButton();

        expect(button.exists()).toBe(true);

        button.vm.$emit('click');

        expect(store.dispatch).toHaveBeenCalledWith(`${allNamespace}/fetchExport`, {
          export_type: EXPORT_FORMAT_JSON_ARRAY,
        });
      });
    });
  });

  describe('when container is an organization', () => {
    beforeEach(() => {
      factory({ props: { container: NAMESPACE_ORGANIZATION } });
    });

    itHasCorrectLoadingLogic(() => findButton());

    it('shows button that dispatches CSV export', () => {
      const button = findButton();

      expect(button.exists()).toBe(true);

      button.vm.$emit('click');

      expect(store.dispatch).toHaveBeenCalledWith(`${allNamespace}/fetchExport`, {
        export_type: EXPORT_FORMAT_CSV,
      });
    });
  });
});
